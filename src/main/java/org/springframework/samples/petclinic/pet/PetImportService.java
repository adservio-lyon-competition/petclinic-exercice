package org.springframework.samples.petclinic.pet;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.file.FileImportException;
import org.springframework.samples.petclinic.owner.IOwnerService;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.treatment.ITreatmentService;
import org.springframework.samples.petclinic.treatment.Treatment;
import org.springframework.samples.petclinic.visit.IVisitService;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Service
public class PetImportService {

	@Autowired
	private IPetService petService;

	@Autowired
	private IOwnerService ownerService;

	@Autowired
	private IVisitService visitService;

	@Autowired
	private ITreatmentService treatmentService;
	
	private static Set<String> unknownMatricule = new HashSet<String>();

	public PetImportJson constructJsonFiles(MultipartFile file) {
		PetImportJson importJsonList = new PetImportJson();
		try {
			importJsonList = new ObjectMapper().readValue(file.getBytes(), PetImportJson.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new FileImportException("Could not import files. Please try again!");
		}

		return importJsonList;
	}

	/**
	 * importPetData data store, either inserting or updating it.
	 * 
	 * @param petImportJson the {@link PetImportJson} to import
	 * @return unknownMatricule list of non-existent veterinarians in DataBase
	 */
	@Transactional
	public Set<String> importPetData(PetImportJson petImportJson) {

		//liste des veterinaires inconnue en BDD
		if (petImportJson.getPetList() != null && petImportJson.getPetList().size() > 0) {
			for (Pet pet : petImportJson.getPetList()) {
				Owner owner = petImportJson.getOwner();
				ownerService.save(owner);
				owner.addPet(pet);
				pet.setTreatmentReport(treatmentService.getTreatmentReport(pet.getTreatments()));
				Pet createdPet = petService.save(pet);
				unknownMatricule.addAll(visitService.saveVisites(createdPet.getId(), pet.getVisits()));
				unknownMatricule.addAll(treatmentService.saveTreatments(createdPet.getId(), pet.getTreatments()));
			}
		}
		return unknownMatricule;

	}

	public String generatePetImportJson(int ownerNumber, int petNumber) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

		PetImportJson petImportJson = initiatePetImportJson(ownerNumber, petNumber);
		String jsonString = "Nothing!";
		try {
			jsonString = mapper.writeValueAsString(petImportJson);

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonString;
	}

	public PetImportJson initiatePetImportJson(int ownerNumber, int petNumber) {

		String[] petTypes = { "cat", "dog", "lizard", "snake", "bird", "hamster", "Horse" };

		Owner owner = null;

		PetImportJson importJson = null;
		List<Pet> petList = new ArrayList<Pet>();

		owner = new Owner();
		Random random = new Random();

		owner.setFirstName("firstName_" + ownerNumber);
		owner.setLastName("lastName_" + ownerNumber);
		owner.setAddress("address_" + ownerNumber);
		owner.setCity("City_" + ownerNumber);
		owner.setTelephone(generatePhoneNumber());

		for (int j2 = 0; j2 < petNumber; j2++) {
			int randomInt = random.nextInt(6);
			PetType petType = new PetType();
			petType.setId(randomInt + 1);
			petType.setName(petTypes[randomInt]);

			Pet petData = getNewPet(j2, petType);

			petList.add(petData);
		}

		importJson = new PetImportJson(owner, petList);

		return importJson;
	}

	private String generatePhoneNumber() {
		Random rand = new Random();
		int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		int num2 = rand.nextInt(743);
		int num3 = rand.nextInt(10000);

		DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

		return df3.format(num1) + df3.format(num2) + df4.format(num3);

	}

	private Pet getNewPet(int petNumber, PetType petType) {

		Pet pet = new Pet();
		pet.setName("petName_" + petNumber);
		pet.setBirthDate(LocalDate.now().minusDays(petNumber));
		pet.setType(petType);
		pet.setVisitsInternal(getVisitsForPet(petNumber));
		pet.setTreatmentsInternal(getTreatmentForPet(petNumber));
		return pet;
	}

	private List<Visit> getVisitsForPet(int j2) {
		List<Visit> visits = new ArrayList<Visit>();
		Random random = new Random();

		for (int i = 0; i < j2 + 2; i++) {
			int randomInt = random.nextInt(6);
			Visit visit = new Visit();

			visit.setDate(LocalDate.now().minusDays(i));
			visit.setDescription("Visit description_" + i);
			visit.setMatriculeVet("matriculeVet" + (randomInt + 1));
			visits.add(visit);
		}

		return visits;
	}

	private static List<Treatment> getTreatmentForPet(int j2) {
		String[] treatmentTypes = { "Tick-borne Diseases", "Heartworms", "Parvo", "animal-assisted therapy",
				"Cognitive", "Physical", "Psychological" };
		Random random = new Random();

		List<Treatment> treatments = new ArrayList<Treatment>();

		for (int i = 0; i < j2 + 2; i++) {
			int randomInt = random.nextInt(6);

			Treatment treatment = new Treatment();
			treatment.setType(treatmentTypes[randomInt]);
			treatment.setDate(LocalDate.now().minusDays(i));
			treatment.setMatriculeVet("matriculeVet" + (randomInt + 1));
			treatment.setDescription("Treatment description for " + treatmentTypes[randomInt] + ".");

			treatments.add(treatment);
		}

		return treatments;
	}

}
