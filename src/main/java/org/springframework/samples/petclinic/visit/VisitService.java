package org.springframework.samples.petclinic.visit;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Service;

@Service
public class VisitService implements IVisitService {

	@Autowired
	private VisitRepository visitRepository;

	@Autowired
	private VetRepository vetRepository;

	@Override
	public List<String> saveVisites(Integer petId, List<Visit> visits) {
		List<String> failVisits = new ArrayList<String>();
		if (visits != null && visits.size() > 0) {
			for (Visit visit : visits) {
				Vet vet = vetRepository.findByMatricule(visit.getMatriculeVet());
				if (null != vet) {
					visit.setVetId(vet.getId());
					visit.setPetId(petId);
					visitRepository.save(visit);
				}else {
					failVisits.add(visit.getMatriculeVet());
				}
			}
		}
		return failVisits;

	}

	@Override
	public List<Visit> findByPetId(Integer petId) {
		return visitRepository.findByPetId(petId);
	}

}
