package org.springframework.samples.petclinic.treatment;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.samples.petclinic.model.BaseEntity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;


@Entity
@Table(name = "treatments")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Treatment extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "treatment_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate date;

	@Column(name = "description")
	private String description;
	
	@Column(name = "treatment_type")
	private String type;

	@Column(name = "pet_id")
	private Integer petId;
	
	@Column(name = "vet_id")
	private Integer vetId;
	
	@Transient
	@NotEmpty(message = "Cannot be empty")
	@Size(max = 30, message = "can not be more than 50 char")
	private String matriculeVet;

	/**
	 * Creates a new instance of Visit for the current date
	 */
	public Treatment() {
		this.date = LocalDate.now();
	}

	public LocalDate getDate() {
		return this.date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPetId() {
		return this.petId;
	}

	public void setPetId(Integer petId) {
		this.petId = petId;
	}
	
	public Integer getVetId() {
		return this.vetId;
	}

	public void setVetId(Integer petId) {
		this.vetId = petId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMatriculeVet() {
		return matriculeVet;
	}

	public void setMatriculeVet(String matriculeVet) {
		this.matriculeVet = matriculeVet;
	}

}
