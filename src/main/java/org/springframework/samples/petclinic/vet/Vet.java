package org.springframework.samples.petclinic.vet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.xml.bind.annotation.XmlElement;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.samples.petclinic.model.Person;
import org.springframework.samples.petclinic.treatment.Treatment;
import org.springframework.samples.petclinic.visit.Visit;

/**
 * Simple JavaBean domain object representing a veterinarian.
 *
 */
@Entity
@Table(name = "vets")
public class Vet extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "matricule")
	@NotEmpty
	private String matricule;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "vet_specialties", joinColumns = @JoinColumn(name = "vet_id"), inverseJoinColumns = @JoinColumn(name = "specialty_id"))
	private Set<Specialty> specialties;

	@Transient
	private Set<Visit> visits = new LinkedHashSet<>();

	@Transient
	private Set<Treatment> treatments = new LinkedHashSet<>();

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	protected Set<Specialty> getSpecialtiesInternal() {
		if (this.specialties == null) {
			this.specialties = new HashSet<>();
		}
		return this.specialties;
	}

	protected void setSpecialtiesInternal(Set<Specialty> specialties) {
		this.specialties = specialties;
	}

	@XmlElement
	public List<Specialty> getSpecialties() {
		List<Specialty> sortedSpecs = new ArrayList<>(getSpecialtiesInternal());
		PropertyComparator.sort(sortedSpecs, new MutableSortDefinition("name", true, true));
		return Collections.unmodifiableList(sortedSpecs);
	}

	public int getNrOfSpecialties() {
		return getSpecialtiesInternal().size();
	}

	public void addSpecialty(Specialty specialty) {
		getSpecialtiesInternal().add(specialty);
	}

	protected Set<Visit> getVisitsInternal() {
		if (this.visits == null) {
			this.visits = new HashSet<>();
		}
		return this.visits;
	}

	protected void setVisitsInternal(Collection<Visit> visits) {
		this.visits = new LinkedHashSet<>(visits);
	}

	public List<Visit> getVisits() {
		List<Visit> sortedVisits = new ArrayList<>(getVisitsInternal());
		PropertyComparator.sort(sortedVisits, new MutableSortDefinition("date", false, false));
		return Collections.unmodifiableList(sortedVisits);
	}

	public void addVisit(Visit visit) {
		getVisitsInternal().add(visit);
		visit.setVetId(this.getId());
	}

	protected Set<Treatment> getTreatmentsInternal() {
		if (this.treatments == null) {
			this.treatments = new HashSet<>();
		}
		return this.treatments;
	}

	protected void setTreatmentsInternal(Collection<Treatment> treatments) {
		this.treatments = new LinkedHashSet<>(treatments);
	}

	public List<Treatment> getTreatments() {
		List<Treatment> sortedTreatments = new ArrayList<>(getTreatmentsInternal());
		PropertyComparator.sort(sortedTreatments, new MutableSortDefinition("date", false, false));
		return Collections.unmodifiableList(sortedTreatments);
	}

	public void addTreatment(Treatment treatment) {
		getTreatmentsInternal().add(treatment);
		treatment.setVetId(this.getId());
	}

}
